#!/usr/local/lib/anaconda/envs/user-management/bin/python

""" cleanusrs
"""

# ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

from argparse import ArgumentParser, Action, ArgumentError
from datetime import datetime, date
import subprocess, sys, os, linecache, logging, logging.handlers

# ---------------------------------------------------------------------------------------------------------------------------------------
# Import file path libraries depending on the operating system; only linux is supported
# ---------------------------------------------------------------------------------------------------------------------------------------
osData = sys.platform.lower()
if osData.startswith('linux'):
    import spwd, pwd, grp
else:
    raise RuntimeError('The current operating system is not supported.')

# ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

this = sys.modules[__name__]
this.g_isVerbose = False
this.g_fileSystem = ''
this.g_group = None
this.g_user = None
this.g_logFile = ''
this.g_logToFile = False
this.g_logger = None
this.g_tmpDir = ''
this.g_cleanTmpDir = False
this.g_mailSpool = ''

# ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

# ---------------------------------------------------------------------------------------------------------------------------------------
# LOGGING
# ---------------------------------------------------------------------------------------------------------------------------------------

class LoggingConsole(object):

    @staticmethod
    def LogError(msg):
        logMsg = "[{datetime}][ERROR]: {message}".format(datetime=str(datetime.now().strftime('%Y/%m/%d %H:%M:%S')),message=msg)
        if not this.g_logToFile:
            print(logMsg)
        else:
            this.g_logger.error(logMsg)

    @staticmethod
    def LogSuccess(msg):
        logMsg = "[{datetime}][SUCCESS]: {message}".format(datetime=str(datetime.now().strftime('%Y/%m/%d %H:%M:%S')), message=msg)
        if not this.g_logToFile and this.g_isVerbose:
            print(logMsg)
        else:
            this.g_logger.info(logMsg)

    @staticmethod
    def LogWarning(msg):
        logMsg = "[{datetime}][WARNING]: {message}".format(datetime=str(datetime.now().strftime('%Y/%m/%d %H:%M:%S')), message=msg)
        if not this.g_logToFile and this.g_isVerbose:
            print(logMsg)
        else:
            this.g_logger.warning(logMsg)

    @staticmethod
    def LogVerbose(msg):
        logMsg = "[{datetime}][VERBOSE]: {message}".format(datetime=str(datetime.now().strftime('%Y/%m/%d %H:%M:%S')), message=msg)
        if not this.g_logToFile and this.g_isVerbose:
            print(logMsg)
        else:
            this.g_logger.info(logMsg)

    @staticmethod
    def GetFormattedException():
        exc_type, exc_obj, tb = sys.exc_info()
        f = tb.tb_frame
        lineno = tb.tb_lineno
        filename = f.f_code.co_filename
        linecache.checkcache(filename)
        line = linecache.getline(filename, lineno, f.f_globals)
        return 'EXCEPTION IN ({}, LINE {} "{}"): {} \r'.format(filename, lineno, line.strip(), exc_obj)

    @staticmethod
    def CreateRotatingLog():
        # Create a logger with a log level set by the user
        this.g_logger = logging.getLogger("Cleanusr Log")
        logLevel = logging.DEBUG if this.g_isVerbose else logging.ERROR
        this.g_logger.setLevel(logLevel)

        # Create a timed rotating file handler that will rotate the logs every Monday (W0).
        # It will only keep 8 backup logs, or 8 weeks of logs.
        handler = logging.handlers.TimedRotatingFileHandler(this.g_logFile, when="W0", backupCount=8)
        this.g_logger.addHandler(handler)

# ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

# ---------------------------------------------------------------------------------------------------------------------------------------
# ARGUMENT PARSING
# ---------------------------------------------------------------------------------------------------------------------------------------

class FilesystemExistsAction(Action):
    """ Verifies that the file exists and is a directory
    """
    def __call__(self, parser, namespace, values, option_string=None):

        if not os.path.isdir(values):
            message = '[ERROR]: The filesystem, {file}, either does not exist or is not a directory.'.format(file=values)
            raise ArgumentError(self, message)

        # Add the attribute
        setattr(namespace, self.dest, values)

class UserExistsAction(Action):
    """ Verifies that the user exists and adds the user object itself as the attribute
    """
    def __call__(self, parser, namespace, values, option_string=None):
        # Verify that the linux user exists
        try:
            user = pwd.getpwnam(values)
        except KeyError:
            message = '[ERROR]: The user, {user}, does not exist.'.format(user=values)
            raise ArgumentError(self, message)

        # Add the attribute
        setattr(namespace, self.dest, user)

class GroupExistsAction(Action):
    """Verifies that the group exists and adds the group object itself as the attribute
    """
    def __call__(self, parser, namespace, values, option_string=None):
        # Verify that the linux group exists
        try:
            group = grp.getgrnam(values)
        except KeyError:
            message = '[ERROR]: The group, {group}, does not exist.'.format(group=values)
            raise ArgumentError(self, message)

        # Add the attribute
        setattr(namespace, self.dest, group)

def get_script_path():
    return os.path.dirname(os.path.realpath(sys.argv[0]))

def collect_args():
    """ Uses an argument parser to gather and parse arguments given by the user in the command line.
    """

    # Default log file location
    logDir = os.path.join(get_script_path(), 'log')

    # Setup arguments to parse
    parser = ArgumentParser(description='Emulates nested grouping for linux machines by adding users from one group to another.')
    parser.add_argument('-fs', '--filesystem', action=FilesystemExistsAction, required=True, help='The filesystem to remark the files of expired users.')
    parser.add_argument('-g', '--group', action=GroupExistsAction, required=True, help='The group to set for file remarking.')
    parser.add_argument('-u', '--user', action=UserExistsAction, required=True, help='The user to set for file remarking.')
    parser.add_argument('-l', '--log', action='store_true', help='Whether or not to log to a file. The logs will rotate every Monday and only 8 log files will be kept at a time.')
    parser.add_argument('-ld', '--logdir', default=logDir, help='If logging to a file, the log file directory to save the log to. By default, it is %(default)s.')
    parser.add_argument('-v', '--verbose', action='store_true', help='Whether or not to log verbose messages and datetime stamps. Errors will still be logged.')
    parser.add_argument('-td','--tmpdir', action=FilesystemExistsAction, help='If this is specified, it will clean out JSON files that are named after the expired users.')
    parser.add_argument('-ms', '--mailspool', action=FilesystemExistsAction, default='/var/spool/mail', help='The directory that the mail spool for users exists. The default is %(default)s.')

    # Parse arguments and return them
    args = vars(parser.parse_args())
    this.g_isVerbose = args['verbose']
    this.g_fileSystem = args['filesystem']
    this.g_group = args['group']
    this.g_user = args['user']
    this.g_logDir = args['logdir']
    this.g_logToFile = args['log']
    this.g_tmpDir = args['tmpdir']
    this.g_cleanTmpDir = bool(args['tmpdir'])
    this.g_mailSpool = args['mailspool']

    # If we're logging, make sure to create all directories needed in the path if they don't exist
    try:
        if this.g_logToFile:
            if not os.path.isdir(this.g_logDir):
                os.makedirs(this.g_logDir)
            this.g_logFile = os.path.join(this.g_logDir, 'cleanusrlog')
    # Don't continue if the log dir couldn't be created
    except Exception as ex:
        if not os.path.isdir(this.g_logDir):
            raise
        else:
            LoggingConsole.LogError("Problem creating the parent log directory. {msg}".format(msg=LoggingConsole.GetFormattedException()))

# ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

# ---------------------------------------------------------------------------------------------------------------------------------------
# CLEANING UP USERS
# ---------------------------------------------------------------------------------------------------------------------------------------

def days_since_epoch(day = date.today()):
    base = date(1970, 1, 1)
    delta = day - base
    return delta.days

def get_expired_users():
    # Since the pwd expire value is days since epoch, we have to get days since epoch for today's date
    numDaysToday = days_since_epoch()

    # Store a dictionary of uids and gids of expired users for faster lookup time when traversing the filesystems
    # Don't add users that have a value of negative 1 to this list since their accounts can't expire
    expiredUsers = dict()
    for SP in spwd.getspall():
        if SP.sp_expire <= numDaysToday and SP.sp_expire != -1:
            uid, gid = get_uid_gid_name(SP.sp_nam)
            expiredUsers[uid] = SP.sp_nam
            expiredUsers[gid] = SP.sp_nam

    return expiredUsers

def remark_file_system(expiredUsers):

    try:
        for root, dirs, files in os.walk(this.g_fileSystem):
            # Get decoded root path
            decodedPath = root.encode('utf-8', 'surrogateescape').decode('ISO-8859-1')

            # Remark files
            for name in files:
                decodedName = name.encode('utf-8', 'surrogateescape').decode('ISO-8859-1')
                fullPath = os.path.join(decodedPath, decodedName)

                # Only chown and chmod it if the uid or gid matches an expired user
                uid, gid = get_uid_gid_path(fullPath)
                if uid in expiredUsers or gid in expiredUsers:
                    os.chown(fullPath, this.g_user.pw_uid, this.g_group.gr_gid)
                    os.chmod(fullPath, 0o0664) # rw- rw- r--
                    LoggingConsole.LogVerbose("Chowning/Chmodding file {filename} with uid {uid} and gid {gid}.".format(filename=fullPath, uid=uid, gid=gid))

            # Remark directories
            for name in dirs:
                decodedName = name.encode('utf-8', 'surrogateescape').decode('ISO-8859-1')
                fullPath = os.path.join(decodedPath, decodedName)

                # Only chown and chmod it if the uid or gid matches an expired user
                uid, gid = get_uid_gid_path(fullPath)
                if uid in expiredUsers or gid in expiredUsers:
                    os.chown(fullPath, this.g_user.pw_uid, this.g_group.gr_gid)
                    os.chmod(fullPath, 0o3775) # SetGID, rwx rwx r-x
                    LoggingConsole.LogVerbose("Chowning/Chmodding directory {filename} with uid {uid} and gid {gid}.".format(filename=fullPath, uid=uid, gid=gid))

    except Exception as ex:
        LoggingConsole.LogError("Problem traversing the file system. {msg}".format(msg=LoggingConsole.GetFormattedException()))

def get_uid_gid_path(fullPath):
    statData = os.stat(fullPath)
    uid = statData.st_uid
    gid = statData.st_gid
    return uid, gid

def get_uid_gid_name(username):
    try:
        pwdData = pwd.getpwnam(username)
    except Exception as ex:
        LoggingConsole.LogError("Problem getting the pwd entry for the user {user}. {msg}".format(user=username, msg=LoggingConsole.GetFormattedException()))
        return None, None

    return pwdData.pw_uid, pwdData.pw_gid

def logout_user(id, username):
    try:
        # Check if user exists first
        user = pwd.getpwnam(username)
        # Logout the user
        subprocess.check_call("pkill -KILL -u {user}".format(user=username), shell=True, stderr=subprocess.STDOUT)
        LoggingConsole.LogVerbose("Logged out and killed all processes for user {user}".format(user=username))
    except KeyError:
        # user doesn't exist; that is OK. Just move on.
        pass
    # Stop the program if something went wrong here; The script cannot run effectively if the expired users are still logged in
    except subprocess.CalledProcessError as ex:
        output = ex.output
        returncode = ex.returncode
        if returncode != 1: # Only raise an error if the return code isn't 1
            LoggingConsole.LogError("Problem logging out and killing all processes for {user}. {msg}".format(user=username, msg=output))
            raise
    except Exception as ex:
        LoggingConsole.LogError("Problem logging out and killing all processes for {user}. {msg}".format(user=username, msg=LoggingConsole.GetFormattedException()))
        raise

def del_expired_user(id, username):
    try:
        # Check if user exists first
        user = pwd.getpwnam(username)
        # Delete the user
        subprocess.check_call("/sbin/userdel {user}".format(user=username), shell=True)
        LoggingConsole.LogVerbose("Deleted user {user}".format(user=username))
    except KeyError:
        # user doesn't exist; that is OK since we might have already deleted the user since there are two entries for one user
        pass
    # Stop the program if something went wrong here; The script cannot run effectively if the expired users are still logged in
    except subprocess.CalledProcessError as ex:
        output = ex.output
        returncode = ex.returncode
        LoggingConsole.LogError("Problem deleting the {user} with returncode {code}. {msg}".format(user=username, code=returncode, msg=output))
    except Exception as ex:
        LoggingConsole.LogError("Problem deleting the {user}. {msg}".format(user=username, msg=LoggingConsole.GetFormattedException()))

def del_expired_user_mailspool(id, username):
    # Get the full path of the temporary JSON file
    userMailSpool = "{username}".format(username=username)
    fullPath = os.path.join(this.g_mailSpool, userMailSpool)

    try:
        # Attempt to remove the temporary JSON file if it exists
        if os.path.exists(fullPath):
            os.remove(fullPath)
            LoggingConsole.LogVerbose("Removed temporary user mail spool {path}".format(path=fullPath))
    except Exception as ex:
        LoggingConsole.LogError("Problem deleting temporary mail spool {path}. {msg}".format(path=fullPath, msg=LoggingConsole.GetFormattedException()))

def clean_tmp_dir(id, username):
    # Get the full path of the temporary JSON file
    tmpUserFile = "{username}.json".format(username=username)
    fullPath = os.path.join(this.g_tmpDir, tmpUserFile)

    try:
        # Attempt to remove the temporary JSON file if it exists
        if os.path.exists(fullPath):
            os.remove(fullPath)
            LoggingConsole.LogVerbose("Removed temporary user JSON file {path}".format(path=fullPath))
    except Exception as ex:
        LoggingConsole.LogError("Problem deleting temporary user JSON file {path}. {msg}".format(path=fullPath, msg=LoggingConsole.GetFormattedException()))

def clean_users():
    # Get a dict of expired users. The keys are the user's UID and GIDs mapped to their username.
    expiredUsers = get_expired_users()
    LoggingConsole.LogVerbose("\n=========================\nProcessing the expired users\n: {users}\n=========================".format(users=expiredUsers))

    # Cleanup all users by logging them out, deleting them and their mailspools, and, if specified, deleting their temporary JSON files
    for id, username in expiredUsers.iteritems():
        logout_user(id, username)
        del_expired_user(id, username)
        del_expired_user_mailspool(id, username)
        if this.g_cleanTmpDir:
            clean_tmp_dir(id, username)

    # Remark all files and directories owned by expired users' UID or GID after cleaning up the user
    remark_file_system(expiredUsers)


# ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

# ---------------------------------------------------------------------------------------------------------------------------------------
# MAIN METHOD
# ---------------------------------------------------------------------------------------------------------------------------------------

def main():
    """ 
    """
    # Collect arguments
    collect_args()

    # Create a rotating log (if specified by user)
    if this.g_logToFile:
        LoggingConsole.CreateRotatingLog()

    # Clean up expired users
    clean_users()

if __name__ == "__main__":
    main()

# ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
