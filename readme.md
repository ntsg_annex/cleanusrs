# cleanusr

## Dependencies

- Linux Only
- Python >= 2.7


## Overview

This python program will clean up expired users and remark their files within a specified filesystem. The following actions will be taken during the execution of this script:

1. The shadow passwd database will be utilized to create a dict of expired users.
2. The subprocess module will be used so that it will logout and kill all processes associated with expired users.
3. The files and directories within the given `filesystem` will be chmodded (0664 for files and 6775 for directories) and chowned such that it matches the specified `user` and `group`.
4. The subprocess module will be used so that it will delete all expired users and their mailspools.
5. If specified, it will delete temporary JSON files named after the user within a specified directory. (This is cleaning up after the "addtmpusr" utility).

It was created to be used as a root chron job in tandem with the "addtmpusr" utility. 

## Usage

```
#!bash

python cleanusrs [-h, --help] [-v, --verbose] [-l, --log]  -fs FILESYSTEM -u USER -g GROUP [-td TMPDIR] [-ld LOGDIR] [-ms MAILSPOOL]
```

| Argument            | Description                                                                                  | Required? | Default    |
| -----------------   | -------------------------------------------------------------------------------------------- | --------- | ---------- |
| `-h`, `--help`      | Show help message and exit.                                                                  | No        | N/A        |
| `-fs, --filesystem` | The filesystem to remark the files of expired users.                                         | Yes       | N/A        |
| `-g, --group`       | The group to set for file remarking.                                                         | Yes       | N/A        |
| `-u, --user`        | The user to set for file remarking.                                                          | Yes       | N/A        |
| `-ms, --mailspool`  | The directory of the user mail spools that will be deleted.                                  | No        | (see below)|
| `-td, --tmpdir`     | If specified, it will clean out JSON fies named after the expired users from here.           | No        | N/A        |
| `-l, --log`         | Whether or not to log the output.                                                            | No        | N/A        |
| `-ld, --logdir`     | The log file directory.                                                                      | No        | (see below)|
| `-v, --verbose`     | Whether or not to log verbose messages and datetime stamps. Errors will still be logged.     | No        | N/A        |

*DEFAULT FOR MAILSPOOL*: `/var/spool/mail`. This has to be done separately (i.e. not automatically with `userdel -r {user}`) since the `-r` switch will delete the user's home directory and this might not be wanted if these users are chrooted SFTP users. In this case, their home directory might be shared.

*DEFAULT FOR LOGDIR*: `path-to-cleanusrs/log`. This program will rotate the logs in this directory every Monday and will only keep 8 logs at a time. Or, in other words, it will only keep 8 weeks of logs around at a time.

In order to get an up-to-date version of the arguments that are passed in to the module, all you have to do is type:
```
#!bash

python cleanusrs --help
```
And you should get a listing of the different arguments that can be passed in.


-------------------------------------------------------------------------------

** Script Author:** Angela Gross